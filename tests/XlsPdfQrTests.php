<?php
namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class DefControllerTest.
 */
class XlsPdfQrTests extends WebTestCase
{
    public function testXls()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('Sign in');
        $form = $button->form([
            'username' => 'admin_admin',
            'password' => 'user321',
        ]);
        $client->submit($form);

        $client->request('GET', '/xls');
        //var_dump($client->getResponse()->getContent());

        $this->assertEquals($client->getResponse()->headers->get('content-type'), 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    }

    public function testPdf()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('Sign in');
        $form = $button->form([
            'username' => 'admin_admin',
            'password' => 'user321',
        ]);
        $client->submit($form);
        $client->request('GET', '/pdf');

        $this->assertEquals($client->getResponse()->headers->get('content-type'), 'application/pdf');
    }

    public function testQr()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('Sign in');
        $form = $button->form([
            'username' => 'admin_admin',
            'password' => 'user321',
        ]);
        $client->submit($form);

        $client->request('GET', '/qr');
        $this->assertEquals($client->getResponse()->headers->get('content-type'), 'image/png');
    }
}
